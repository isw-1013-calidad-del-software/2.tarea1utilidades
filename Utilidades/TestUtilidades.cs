using NUnit.Framework;
using System;

namespace Utilidades
{
    [TestFixture]
    public class Tests
    {
        // Pruebas Unitarias de Utilidades.dividir
        [Test]
        public void DividirTest1()
        {
            //Arrange
            double dividendo = 10;
            double divisor = 2;

            //Act
            double result = Utilidades.dividir(dividendo, divisor);

            //Assert
            Assert.AreEqual(5, result);
        }

        [Test]
        public void DividirTest2()
        {
            //Arrange
            double dividendo = 5;
            double divisor = 2;

            //Act
            double result = Utilidades.dividir(dividendo, divisor);

            //Assert
            Assert.AreEqual(2.5, result);
        }

        [Test]
        public void DividirTest3()
        {
            //Arrange
            double dividendo = 10;
            double divisor = -2;

            //Act
            double result = Utilidades.dividir(dividendo, divisor);

            //Assert
            Assert.AreEqual(-5, result);
        }

        [Test]
        public void DividirTest4()
        {
            //Arrange
            double dividendo = 10;
            double divisor = 0;

            //Act
            double result = Utilidades.dividir(dividendo, divisor);

            //Assert
            Assert.AreEqual(0, result);
            //Assert.AreNotEqual(0, result);
        }

        // Pruebas Unitarias de Utilidades.dividir
        [Test]
        public void ExtraerNumeroTest1()
        {
            //Arrange
            string cadena = "abcdefghik=jklmnopjq5";

            //Act
            double result = Utilidades.extraerNumero(cadena);

            //Assert
            Assert.AreEqual(5, result);
        }
        [Test]
        public void ExtraerNumeroTest2()
        {
            //Arrange
            string cadena = "abcd0efghik1=j4kl8no7pjq0";

            //Act
            double result = Utilidades.extraerNumero(cadena);

            //Assert
            Assert.AreEqual(0, result);
        }
        [Test]
        public void ExtraerNumeroTest3()
        {
            //Arrange
            string cadena = "-1ksdfjuehfdll";

            //Act
            double result = Utilidades.extraerNumero(cadena);

            //Assert
            Assert.AreEqual(1, result);
        }
        [Test]
        public void ExtraerNumeroTest4()
        {
            //Arrange
            string cadena = "sdhfkjskdjhjkhsd";

            //Act
            double result = Utilidades.extraerNumero(cadena);

            //Assert
            Assert.AreEqual(-1, result);
        }
    }
}