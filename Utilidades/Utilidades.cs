﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Utilidades
{
    class Utilidades
    {
        public Utilidades()
        {

        }
        //Crear un método llamado división que realice la división entera entre dos números public int 
        //division(int dividendo, int divisor)
        public static double dividir(double dividendo, double divisor)
        {
            double resultado;
            if (divisor == 0)
            {
                resultado = 0;
            } else
            {
                resultado = dividendo / divisor;
            }
            
            return resultado;
        }

        //método llamado extraerNumero que dada una cadena de texto nos devuelva el
        //primer número que encuentre en ella. public int extraerNumero(string cadena)
        public static double extraerNumero(string cadena)
        {
            double result = -1;
            //char[] caracteres = new char[cadena.Length];
            char[] caracteres = cadena.ToCharArray();

            foreach (char caracter in caracteres)
            {
                if (Char.IsNumber(caracter) == true)
                {
                    result = Char.GetNumericValue(caracter);
                    break;
                }
            }

            return result;
        }
    }
}
